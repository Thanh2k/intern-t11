<?php 
namespace App\Repositories;

use App\Repositories\EloquentRepository;

/**
 * 
 */
class DetailRepository extends EloquentRepository
{
	public function getModel()
	{
		return \App\Models\Detail::class;
	}
}