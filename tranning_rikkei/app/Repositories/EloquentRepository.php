<?php 
namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Carbon\Carbon;

abstract class EloquentRepository implements RepositoryInterface
{
	protected $model;

	/**
    * EloquentRepository constructor.
    */
	function __construct()
	{
		$this->setModel();
	}

	abstract public function getModel();

	public function setModel()
	{
		$this->model = app()->make($this->getModel());
	}

	/**
    * Get All
    * @return \Illuminate\Database\Eloquent\Collection|static[]
    */
	public function getAll()
	{
		return $this->model->all();
	}

	/**
    * Get One
    * @param $id
    * @return mixed
    */
	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	/**
    * Create
    * @param arr $data
    * @return mixed
    */
	public function create(array $data)
	{
		return $this->model->create($data);
	}

	/**
    * Update
    * @param arr $data
    * @param $id
    * @return bool|mixed
    */
	public function update(array $data, $id)
	{
		$result = $this->find($id);
		if ($result) {
			$result->update($data);
			return $result;
		}
		return false;
	}

	/**
    * Delete
    * @param $id
    * @return bool
    */
	public function delete($id)
	{
		$result = $this->find($id);
		if ($result) {
			$result->delete($id);
			return true;
		}
		return false;
	}
}