<?php 
namespace App\Repositories\Contracts;

interface RepositoryInterface{
	/**
    * Get all
    * @return mixed
    */
	public function getAll();

	/**
    * Get one
    * @param $id
    * @return mixed
    */
	public function find($id);

	/**
    * Create
    * @param arr $data
    * @return mixed
    */
	public function create(array $data);

	/**
    * Update
    * @param arr $data
    * @param $id
    * @return mixed
    */
	public function update(array $data, $id);

	/**
    * Delete
    * @param $id
    * @return mixed
    */
	public function delete($id);
}
