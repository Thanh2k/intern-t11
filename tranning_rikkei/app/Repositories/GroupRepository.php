<?php 
namespace App\Repositories;

use App\Repositories\EloquentRepository;

/**
 * 
 */
class GroupRepository extends EloquentRepository
{
	public function getModel()
	{
		return \App\Models\Group::class;
	}
}