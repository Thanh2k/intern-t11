<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = [
        'name', 'user_id', 'avatar', 'status', 'created_at','updated_at',
    ];

    public function detail()
    {
        return $this->hasMany('App\Models\Detail');
    }
}
