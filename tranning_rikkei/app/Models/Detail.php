<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = 'details';

    protected $fillable = [
        'name', 'list_id', 'description', 'start_date', 'finish_date', 'time_remind', 'content_remind', 'status', 'status_time_remind', 'created_at','updated_at'
    ];
}
