<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RequestLogin;
use Auth;

class LoginController extends Controller
{
    //Hàm chuyển sang view login
    public function login()
    {
        return view('auth.login');
    }

    //Hàm post form login
    public function postLogin(RequestLogin $request)
    {
        if (Auth::attempt($request->only('email', 'password'), $request->has('remember'))) {
            return redirect()->route('home');
        }else{
            return redirect()->back()->with('error','Email or Password incorrect !');
        }
    }
}
