<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\RequestRegister;
use App\User;
use Auth;

class RegisterController extends Controller
{
    //Hàm chuyển sang view register
    public function register()
    {
        return view('auth.register');
    }

    //Hàm post form đăng ký
    public function postRegister(RequestRegister $request)
    {
        $user = new User();
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('login')->with('success','Register an account successfully. Let\'s login !!!');
    }
}
