<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RequestForgotPass;
use App\User;
use Auth;
use Mail;

class ForgotPasswordController extends Controller
{
    //Hàm chuyển sang view forgotpass
    public function forgotPassword()
    {
        return view('auth.forgotpass');
    }

    //Hàm sinh ra password ngẫu nhiên
    public function randomPassword($length, $allow = "abcdefghijklmnopqrstuvwxyz0123456789") 
    {
        $i = 1;
        $ret = 1;
        while ($i <= $length) {
            $max = strlen($allow)-1;
            $num = rand(0, $max);
            $temp = substr($allow, $num, 1);
            $ret .= $temp;
            $i++;
        }
        return $ret;
    }

    //Hàm post form forgot password
    public function postForgotPassword(RequestForgotPass $request)
    {
        $user = User::where(['email' => $request->email])->first();
        $newPass = $this->randomPassword(10);
        $newPassHashed = bcrypt($newPass);
        $user->password = $newPassHashed;
        $user->save();
        $this->sendMail($user, $newPass);

        return redirect()->route('login')->with('success','The new password sent to your email !!!');
    }

    //Hàm gửi email
    public function sendMail($user, $newPass)
    {
        Mail::send(
            'emails.forgotpass',
            [
                'user' => $user,
                'newPass' => $newPass
            ],
            function($message) use ($user)
            {
                $message->to($user->email);
                $message->subject("$user->name, reset your password.");
            }
        );
    }
}
