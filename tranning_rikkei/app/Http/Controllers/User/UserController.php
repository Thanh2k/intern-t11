<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestChangePassword;
use App\Http\Requests\RequestEditProfile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use Auth;

class UserController extends Controller
{
    //Hàm chuyển sang view profile user
    public function profile()
    {
        $user = User::where(['id' => Auth::id()])->first();
        return view('user.profile',compact(['user']));
    }

    //Hàm chuyển sang view edit profile user
    public function editProfile()
    {
        $user = User::where(['id' => Auth::id()])->first();
        return view('user.editprofile',compact(['user']));
    }

    //Hàm post form edit profile user
    public function postEditProfile(RequestEditProfile $request)
    {
        $id = Auth::id();
        $request->offsetUnset('_token');
        $user = User::where('id', '<>', $id)->get();
        foreach ($user as $u) {
            if($request->name == $u->name){
                $request->session()->flash('error', 'Name already exists');
                return redirect()->back();
            }
            if($request->email == $u->email){
                $request->session()->flash('error', 'Email address already exists');
                return redirect()->back();
            }
            if($request->phone_number == $u->phone_number && $request->phone_number != null){
                $request->session()->flash('error', 'Phone number already exists');
                return redirect()->back();
            }
        }
        User::where(['id' => $id])->update($request->all());
        return redirect()->route('profile');
    }

    //Hàm chuyển sang view edit password user
    public function changePass()
    {
        $user = User::where(['id' => Auth::id()])->first();
        return view('user.changepass',compact(['user']));
    }

    //Hàm post form edit password user
    public function postChangePass(RequestChangePassword $request)
    {
        $id = Auth::id();
        $user = User::find($id);
        if(!Hash::check($request->old_password, $user->password)){
            $request->session()->flash('error', 'Password does not match with your old password');
            return redirect()->back();
        }
        if($request->old_password == $request->password){
            $request->session()->flash('error', 'The new password must be different from the current password');
            return redirect()->back();
        }
        
        //Hash password
        $password = bcrypt($request->password);
        //Merge lại
        $request->merge(['password'=>$password]);
        User::where(['id' => $id])->update($request->only('password'));
        $request->session()->flash('success', 'Password changed !!!');
        return redirect()->route('profile');
    }
}
