<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:150|unique:users',
            'email' => 'required|email|max:150|unique:users',
            'password' => 'required|min:8|max:50',
            'password_confirmation' => 'required|min:8|max:50|same:password'
        ];
    }

    public function messages()
    {
        return [
            'password_confirmation.same' => 'Password and Confirm password not match !',
        ];
    }
}
