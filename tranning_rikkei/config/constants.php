<?php 

return [
	'options' => [
		'WORK_DEFAULT_STATUS' => '0',
		'WORKING_STATUS' => '1',
		'WORKED_STATUS' => '2',
		'TURN_ON_TIME_REMIND' => '1',
		'TURN_OFF_TIME_REMIND' => '0',
	],
	'string' => [
		'EMPTY' => '',
	]
];