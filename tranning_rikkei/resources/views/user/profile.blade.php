@extends('master')
@section('content')
<div class="content">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>{!! Session::get('success') !!}</strong> 
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-user"></i><strong class="card-title pl-2">Your Profile</strong>
                    </div>
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <img class="rounded-circle mx-auto d-block" src="images/face-default.jpg" alt="Card image cap">
                            <h3 class="text-sm-center mt-2 mb-1" style="color: #64925b;">{{$user->name}}</h3>
                            <div class="location text-sm-center"><i class="fa fa-envelope-o"></i> {{$user->email}}</div>
                            <div class="location text-sm-center"><i class="fa fa-map-marker"></i> {{(!empty($user->address)) ? $user->address : 'Not updated'}}</div>
                            <div class="location text-sm-center"><i class="fa fa-volume-control-phone"></i> {{(!empty($user->phone_number)) ? $user->phone_number : 'Not updated'}}</div>
                            <div style="margin-left: 68%;margin-top: 3%;">
                                <a href="{{ route('edit_profile') }}" class="btn btn-outline-primary btn-sm">Update Infor</a>
                                <a href="{{ route('change_pass') }}" class="btn btn-outline-danger btn-sm" style="margin-left: 2%">Change password</a>
                            </div>
                        </div>
                        <hr>
                        <div class="card-text text-sm-center">
                            <a href="#"><i class="fa fa-facebook pr-1"></i></a>
                            <a href="#"><i class="fa fa-twitter pr-1"></i></a>
                            <a href="#"><i class="fa fa-linkedin pr-1"></i></a>
                            <a href="#"><i class="fa fa-pinterest pr-1"></i></a>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- .row -->
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
