@extends('master')
@section('content')
<div class="content" style="margin: 0 0 8% 18%;">
    <div class="animated fadeIn">
        <div class="row">

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header text-center">
                            <strong>Change Password</strong>
                        </div>
                        <div class="card-body card-block">
                            <div id="error" style="display: none">{{session('error')}}</div>
                            <form method="POST" action="{{ route('change_pass') }}">
                                @csrf
                                <div class="has-success form-group">
                                    <label for="inputSuccess2i" class=" form-control-label">Old password <small>(*)</small></label>
                                    <input id="name" type="password" class="form-control" name="old_password" value="{{old('old_password')}}">
                                    <div style="color: red;">
                                        @if($errors->has('old_password'))
                                            {{ $errors->first('old_password') }}
                                        @endif
                                    </div>
                                </div>

                                <div class="has-warning form-group">
                                    <label for="inputWarning2i" class=" form-control-label">New Password <small>(*)</small></label>
                                    <input id="name" type="password" class="form-control" name="password" value="{{ old('password') }}">
                                    <div style="color: red;">
                                        @if($errors->has('password'))
                                            {{ $errors->first('password') }}
                                        @endif
                                    </div>
                                </div>

                                <div class="has-danger has-feedback form-group">
                                    <label for="inputError2i" class=" form-control-label">Confirm password <small>(*)</small></label>
                                    <input id="name" type="password" class="form-control" name="confirm_password" value="{{ old('confirm_password') }}">
                                    <div style="color: red;">
                                        @if($errors->has('confirm_password'))
                                            {{ $errors->first('confirm_password') }}
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0" style="margin-left: 20%;padding-top: 2% !important;">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-sm btn-primary">
                                            Update
                                        </button>
                                        <a href="{{ route('profile') }}" class="btn btn-sm btn-warning" style="margin-left: 11%;">Back</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

        </div><!-- .row -->
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
@stop