@extends('master')
@section('content')
<div class="content" style="margin: 0 0 8% 18%;">
    <div class="animated fadeIn">
        <div class="row">

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header text-center">
                            <strong>Update Information</strong>
                        </div>
                        <div class="card-body card-block">
                            <div id="error" style="display: none">{{session('error')}}</div>
                            <form method="POST" action="{{ route('edit_profile') }}">
                                @csrf
                                <div class="has-success form-group">
                                    <label for="inputSuccess2i" class=" form-control-label">Full name <small>(*)</small></label>
                                    <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}">
                                    <div style="color: red;">
                                        @if($errors->has('name'))
                                            {{ $errors->first('name') }}
                                        @endif
                                    </div>
                                </div>

                                <div class="has-warning form-group">
                                    <label for="inputWarning2i" class=" form-control-label">Email <small>(*)</small></label>
                                    <input id="name" type="text" class="form-control" name="email" value="{{$user->email}}">
                                    <div style="color: red;">
                                        @if($errors->has('email'))
                                            {{ $errors->first('email') }}
                                        @endif
                                    </div>
                                </div>

                                <div class="has-danger has-feedback form-group">
                                    <label for="inputError2i" class=" form-control-label">Address</label>
                                    <input id="name" type="text" class="form-control" max="200" name="address" value="{{$user->address}}">
                                    <div style="color: red;">
                                        @if($errors->has('address'))
                                            {{ $errors->first('address') }}
                                        @endif
                                    </div>
                                </div>

                                <div class="has-danger has-feedback form-group">
                                    <label for="inputError2i" class=" form-control-label">Phone number</label>
                                    <input id="name" type="number" class="form-control" name="phone_number" value="{{$user->phone_number}}">
                                    <div style="color: red;">
                                        @if($errors->has('phone_number'))
                                            {{ $errors->first('phone_number') }}
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group row mb-0" style="margin-left: 20%;padding-top: 2% !important;">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-outline-success btn-sm">
                                            Update
                                        </button>
                                        <a href="{{ route('profile') }}" class="btn btn-outline-danger btn-sm" style="margin-left: 13%;">Back</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

        </div><!-- .row -->
    </div><!-- .animated -->
</div><!-- .content -->
@endsection

@section('js')
@stop