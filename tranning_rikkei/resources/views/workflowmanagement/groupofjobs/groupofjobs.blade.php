@extends('master')
@section('content')
<div class="content" style="margin: 0 0 30% 3%;">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-11">
                <a href="{{ route('create_group') }}" class="btn btn-outline-success btn-sm pull-right"><i class="fa fa-plus"></i>&nbsp;Create Group of jobs</a>
            </div>
            <div class="col-lg-11">
                <br>
                <div class="card">
                    <div class="card-header">
                        <strong>Group of jobs</strong>
                    </div>
                    <div class="card-body" style="padding-bottom: 0 !important;">
                        @forelse($groups as $g)
                            <a href="#">
                                <img class="img-responsive avatar" src="/img/avatar/{{$g->avatar}}" alt=""><h5 class="groupofjob" title="{{$g->name}}">{{$g->name}}</h5>
                            </a>
                            <a href="{{ route('delete_group',['id'=>$g->id]) }}" class="delete-button" onclick="return confirm('Are you sure you want to delete this workgroup ?')"><span class="fa fa-trash"></span></a>
                            <a href="{{ route('edit_group',['id'=>$g->id]) }}" class="edit-button"><span class="fa fa-pencil"></span></a>
                            <a href="{{ route('work_list',['groupId'=>$g->id]) }}" class="list-button"><span class="fa fa-list"></span></a>
                            <br>
                            <br>
                        @empty
                            <h4 style="color: red;margin-bottom: 4%;">You do not have any group of jobs !</h4>
                        @endforelse
                    </div>
                </div><!-- /# card -->
            </div><!--  /.col-lg-6 -->

        </div><!-- .row -->
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@section('css')
<link rel="stylesheet" href="css/style.css">
@stop