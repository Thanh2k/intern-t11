@extends('master')
@section('content')
<div class="content" style="margin: 0 0 30% 0;">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong>Things to do</strong>
                    </div>
                    <div class="card-body" style="padding-bottom: 0 !important;">
                        @forelse($details as $d)
                        <a href="" data-toggle="modal" data-target="#a{{$d->id}}">
                            <h5 class="worklist" title="{{$d->name}}">{{ucwords($d->name)}}</h5><i class="{{$d->time_remind ? 'fa fa-bell remind': null}} "></i>
                        </a>
                        <a href="{{ route('delete_list',['id' => $d->id]) }}" class="delete" onclick="return confirm('Are you sure you want to delete this job ?')"><span class="fa fa-trash"></span></a>
                        <a href="{{ route('edit_list',['groupId' => $groupId, 'id' => $d->id]) }}" class="edit"><span class="fa fa-pencil"></span></a>
                        <a href="" class="info" data-toggle="modal" data-target="#a{{$d->id}}"><span class="fa fa-eye"></span></a>
                        <br>
                        <br>

                        <!-- .model info -->
                        <div class="modal fade" id="a{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="smallmodalLabel">{{ucwords($d->name)}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p><strong class="model-tittle">Description : </strong>{{$d->description ? $d->description : 'Write a few characters !'}}</p>
                                        <p><strong class="model-tittle">Start date : </strong>{{$d->start_date ? date('m-d-Y H:i', strtotime($d->start_date)) : 'Let\'s set up start date !'}}</p>
                                        <p><strong class="model-tittle">Finish date : </strong>{{$d->finish_date ? date('m-d-Y H:i', strtotime($d->finish_date)) : 'Let\'s set up finish date !'}}</p>
                                        <p><strong class="model-tittle">Time remind : </strong>{{$d->time_remind ? date('m-d-Y H:i', strtotime($d->time_remind)) : 'Let\'s set up time remind !'}}</p>
                                        <p><strong class="model-tittle">Content remind : </strong>{{$d->content_remind ? $d->content_remind : 'Write a few characters !'}}</p>

                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{ route('edit_list',['groupId' => $groupId, 'id' => $d->id]) }}" class="btn btn-sm btn-info"><i class="fa fa-pencil-square"></i>&nbsp; Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                        <h4 style="color: #dc3807;margin-bottom: 6%;">Let's create new thing to do !</h4>
                        @endforelse
                        <div class="col-md-11" style="padding-bottom: 22%;margin-left: 14%;">
                            <a href="{{ route('create_list',['groupId'=>$groupId]) }}" class="btn btn-outline-success btn-sm pull-right"><i class="fa fa-plus"></i>&nbsp;Create</a>
                        </div>
                    </div>
                </div><!-- /# card -->
            </div><!--  /.col-lg-4 -->

            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong>Doing</strong>
                    </div>
                    <div class="card-body" style="padding-bottom: 0 !important;">
                        @forelse($listDoing as $lD)
                        <a href="" data-toggle="modal" data-target="#a{{$lD->id}}">
                            <h5 class="worklist" title="{{$lD->name}}">{{ucwords($lD->name)}}</h5><i class="{{$lD->time_remind ? 'fa fa-bell remind': null}} "></i>
                        </a>
                        <a href="{{ route('delete_list',['id' => $lD->id]) }}" class="delete" onclick="return confirm('Are you sure you want to delete this job ?')"><span class="fa fa-trash"></span></a>
                        <a href="{{ route('edit_list',['groupId' => $groupId, 'id' => $lD->id]) }}" class="edit"><span class="fa fa-pencil"></span></a>
                        <a href="" class="info" data-toggle="modal" data-target="#a{{$lD->id}}"><span class="fa fa-eye"></span></a>
                        <br>
                        <br>

                        <!-- .model info -->
                        <div class="modal fade" id="a{{$lD->id}}" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="smallmodalLabel">{{ucwords($lD->name)}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p><strong class="model-tittle">Description : </strong>{{$lD->description ? $lD->description : 'Write a few characters !'}}</p>
                                        <p><strong class="model-tittle">Start date : </strong>{{$lD->start_date ? date('m-d-Y H:i', strtotime($lD->start_date)) : 'Let\'s set up start date !'}}</p>
                                        <p><strong class="model-tittle">Finish date : </strong>{{$lD->finish_date ? date('m-d-Y H:i', strtotime($lD->finish_date)) : 'Let\'s set up finish date !'}}</p>
                                        <p><strong class="model-tittle">Time remind : </strong>{{$lD->time_remind ? date('m-d-Y H:i', strtotime($lD->time_remind)) : 'Let\'s set up time remind !'}}</p>
                                        <p><strong class="model-tittle">Content remind : </strong>{{$lD->content_remind ? $lD->content_remind : 'Write a few characters !'}}</p>

                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{ route('edit_list',['groupId' => $groupId, 'id' => $lD->id]) }}" class="btn btn-sm btn-info"><i class="fa fa-pencil-square"></i>&nbsp; Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                        <h4 style="color: #bd6044;margin-bottom: 6%;">I am waitting !</h4>
                        @endforelse
                    </div>
                </div><!-- /# card -->
            </div><!--  /.col-lg-4 -->

            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong>Done</strong>
                    </div>
                    <div class="card-body" style="padding-bottom: 0 !important;">
                        @forelse($listDone as $done)
                        <a href="" data-toggle="modal" data-target="#a{{$done->id}}">
                            <h5 class="worklist" title="{{$done->name}}">{{ucwords($done->name)}}</h5><i class="{{$done->time_remind ? 'fa fa-bell remind': null}} "></i>
                        </a>
                        <a href="{{ route('delete_list',['id' => $done->id]) }}" class="delete" onclick="return confirm('Are you sure you want to delete this job ?')"><span class="fa fa-trash"></span></a>
                        <a href="{{ route('edit_list',['groupId' => $groupId, 'id' => $done->id]) }}" class="edit"><span class="fa fa-pencil"></span></a>
                        <a href="" class="info" data-toggle="modal" data-target="#a{{$done->id}}"><span class="fa fa-eye"></span></a>
                        <br>
                        <br>

                        <!-- .model info -->
                        <div class="modal fade" id="a{{$done->id}}" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="smallmodalLabel">{{ucwords($done->name)}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p><strong class="model-tittle">Description : </strong>{{$done->description ? $done->description : 'Write a few characters !'}}</p>
                                        <p><strong class="model-tittle">Start date : </strong>{{$done->start_date ? date('m-d-Y H:i', strtotime($done->start_date)) : 'Let\'s set up start date !'}}</p>
                                        <p><strong class="model-tittle">Finish date : </strong>{{$done->finish_date ? date('m-d-Y H:i', strtotime($done->finish_date)) : 'Let\'s set up finish date !'}}</p>
                                        <p><strong class="model-tittle">Time remind : </strong>{{$done->time_remind ? date('m-d-Y H:i', strtotime($done->time_remind)) : 'Let\'s set up time remind !'}}</p>
                                        <p><strong class="model-tittle">Content remind : </strong>{{$done->content_remind ? $done->content_remind : 'Write a few characters !'}}</p>

                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{ route('edit_list',['groupId' => $groupId, 'id' => $done->id]) }}" class="btn btn-sm btn-info"><i class="fa fa-pencil-square"></i>&nbsp; Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                        <h4 style="color: #bb8373;margin-bottom: 6%;">I am waitting, too !</h4>
                        @endforelse
                    </div>
                </div><!-- /# card -->
            </div><!--  /.col-lg-4 -->

        </div><!-- .row -->
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
@stop