@extends('master')
@section('content')
<div class="content" style="margin: 1% 0 20% 3%;">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header"><strong>Create work list</strong></div>
                    <div class="card-body">
                        <div id="error" style="display: none">{{session('error')}}</div>
                        <form method="POST" action="{{ route('create_list',['groupId' => $groupId]) }}" enctype="multipart/form-data" class="form-horizontal">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 text-md-right form-control-label">Name <small>(*)</small></label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    <div style="color: red;">
                                        @if($errors->has('name'))
                                            {{ $errors->first('name') }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 text-md-right form-control-label">Description</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="description" value="{{ old('description') }}">
                                    <div style="color: red;">
                                        @if($errors->has('description'))
                                            {{ $errors->first('description') }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 text-md-right form-control-label">Start date</label>

                                <div class="col-md-6">
                                    <input id="name" type="datetime-local" class="form-control" name="start_date" value="{{ old('start_date') }}">
                                    <div style="color: red;">
                                        @if($errors->has('start_date'))
                                            {{ $errors->first('start_date') }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 text-md-right form-control-label">Finish date</label>

                                <div class="col-md-6">
                                    <input id="name" type="datetime-local" class="form-control" name="finish_date" value="{{ old('finish_date') }}">
                                    <div style="color: red;">
                                        @if($errors->has('finish_date'))
                                            {{ $errors->first('finish_date') }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 text-md-right form-control-label">Time remind</label>

                                <div class="col-md-6">
                                    <input id="name" type="datetime-local" class="form-control" name="time_remind" value="{{ old('time_remind') }}">
                                    <div style="color: red;">
                                        @if($errors->has('time_remind'))
                                            {{ $errors->first('time_remind') }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 text-md-right form-control-label">Content remind</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="content_remind" value="{{ old('content_remind') }}">
                                    <div style="color: red;">
                                        @if($errors->has('content_remind'))
                                            {{ $errors->first('content_remind') }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0" style="margin-left: 30%;padding-top: 2% !important;">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-sm btn-info">
                                        Create
                                    </button>
                                    <a href="{{ route('work_list',['groupId'=>$groupId]) }}" class="btn btn-sm btn-danger" style="margin-left: 9%;">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div><!-- .row -->
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop
@section('js')
    <script></script>
@stop
