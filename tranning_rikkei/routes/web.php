<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::namespace('Auth')->group(function ()
{
	Route::get('/login', 'LoginController@login')->name('login');
	Route::post('/login', 'LoginController@postLogin')->name('login');
	Route::get('/register', 'RegisterController@register')->name('register');
	Route::post('/register', 'RegisterController@postRegister')->name('register');
	Route::get('/logout', 'LogoutController@postLogout')->name('logout');
	Route::get('/forgot_pass', 'ForgotPasswordController@forgotPassword')->name('forgot_pass');
	Route::post('/forgot_pass', 'ForgotPasswordController@postForgotPassword')->name('forgot_pass');
});

Route::namespace('User')->group(function ()
{
	Route::get('/profile', 'UserController@profile')->name('profile');
	Route::get('/edit-profile', 'UserController@editProfile')->name('edit_profile');
	Route::post('/edit-profile', 'UserController@postEditProfile')->name('edit_profile');
	Route::get('/change-pass', 'UserController@changePass')->name('change_pass');
	Route::post('/change-pass', 'UserController@postChangePass')->name('change_pass');
});

include "workflowmanagements.php";



