<?php 
	Route::namespace('WorkflowManagement')->group(function ()
	{
		//Route tạo nhóm công việc
		Route::get('/group-of-jobs', 'GroupOfJobsController@groupOfJobs')->name('group_of_jobs');
		Route::get('/create-group', 'GroupOfJobsController@createGroup')->name('create_group');
		Route::post('/create-group', 'GroupOfJobsController@postCreateGroup')->name('create_group');
		Route::get('/edit-group/{id}', 'GroupOfJobsController@editGroup')->name('edit_group');
		Route::post('/edit-group/{id}', 'GroupOfJobsController@postEditGroup')->name('edit_group');
		Route::get('/delete-group/{id}', 'GroupOfJobsController@deleteGroup')->name('delete_group');

		//Route tạo danh sách cho nhóm công việc
		Route::get('/work-list/{groupId}', 'WorkListController@workList')->name('work_list');
		Route::get('/create-work-list/{groupId}', 'WorkListController@createWorkList')->name('create_list');
		Route::post('/create-work-list/{groupId}', 'WorkListController@postCreateWorkList')->name('create_list');
		Route::get('/edit-work-list/{groupId}/{id}', 'WorkListController@editWorkList')->name('edit_list');
		Route::post('/edit-work-list/{groupId}/{id}', 'WorkListController@postEditWorkList')->name('edit_list');
		Route::get('/delete-work-list/{id}', 'WorkListController@deleteWorkList')->name('delete_list');
	});
 ?>